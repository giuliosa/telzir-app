# TelzirApp

Made by Giulio Sá, for a code challenge

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.3.

## Development server

First, run `npm install` for install all the dependencies. Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## BackEnd server

Go to the backend folder using `cd backend` and run a `npm install` for install all the dependencies. After that, run `npm start`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

import { Routes } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { OffersComponent } from './offers/offers.component';

export const ROUTES: Routes = [
    { path: '', component: LandingComponent },
    { path: 'ofertas', component: OffersComponent}
]
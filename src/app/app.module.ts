import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { LandingComponent } from './landing/landing.component';
import { ROUTES } from './app.routes';
import { RouterModule } from '@angular/router';
import { OffersComponent } from './offers/offers.component';
import { ReactiveFormsModule } from '@angular/forms';
import { OffersService } from './offers/offers.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    OffersComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [OffersService],
  bootstrap: [AppComponent]
})
export class AppModule { }

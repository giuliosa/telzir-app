class Offer {
    constructor(
        public origin: string,
        public destiny: string,
        public time: number,
        public plan: string
    ) {}
}


export {Offer};
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Offer } from './offer.model';
import { OffersService } from './offers.service';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {

  public offersForm: FormGroup;
  public plan;
  public currentOffer: Offer;

  constructor(private formBuilder: FormBuilder, private offers: OffersService) { }

  ngOnInit(): void {
    this.offersForm = this.formBuilder.group({
      origin: this.formBuilder.control('', Validators.required),
      destiny: this.formBuilder.control('', Validators.required),
      time: this.formBuilder.control('', Validators.required),
      plan: this.formBuilder.control('', Validators.required)
    }, {validator: [OffersComponent.equalsTo]})

    
  }

  static equalsTo(group: AbstractControl): {[key: string]: boolean} {
    const origin = group.get('origin');
    const destiny = group.get('destiny');

    if(!origin || !destiny) {
      return undefined;
    }

    if(origin.value == destiny.value){
      return {equals: true}
    }

    return undefined
  }

  calculateCosts(offer: Offer){
    this.currentOffer = offer;
    this.offers.checkOffers(offer).subscribe((value) => {
      this.plan = value;
      console.log(this.plan);
    })
  }

}

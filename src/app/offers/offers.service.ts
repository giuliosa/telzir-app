import { Injectable } from '@angular/core';
import {HttpClient, HttpParams } from '@angular/common/http';
import { Offer } from './offer.model';

@Injectable()
export class OffersService {
    constructor(private http: HttpClient){}

    checkOffers(offer: Offer){
        let params: HttpParams = new HttpParams();
        if(offer){
            params = params.set('origin', offer.origin);
            params = params.set('destiny', offer.destiny);
            params = params.set('time', offer.time.toString());
            params = params.set('offers', offer.plan);
        }
        return this.http.get<Offer>(`http://localhost:3333/offers`, {params:params});
    }
}
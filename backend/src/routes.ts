import express from 'express';

import OffersController from './controller/OffersController';

const routes = express.Router();

const offersController = new OffersController();
routes.get('/offers', offersController.index);

export default routes;
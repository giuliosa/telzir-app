function returnDDDPrice(origin: string, destiny: string){
    if (origin === '11' && destiny === '16') {
        return 1.90
    } else if(origin === '16' && destiny === '11') {
        return 2.90
    } else if(origin === '11' && destiny === '17') {
        return 1.70
    } else if(origin === '17' && destiny === '11') {
        return 2.70
    } else if(origin === '11' && destiny === '18') {
        return 0.90
    } else if(origin === '18' && destiny === '11') {
        return 1.90
    }
    return 0
}

function calculatePrice(price: number, time: number, offer: string){
    const objetoLouco = {
        calculatedPrice: 0,
        otherPrice: 0
    }

    switch (offer) {
        case '30':
            if(time > 30){                
                objetoLouco.calculatedPrice = price * (time - 30) ;
                objetoLouco.calculatedPrice = objetoLouco.calculatedPrice + ((objetoLouco.calculatedPrice) * 10)/100
            } 
            objetoLouco.otherPrice = (price * time)        
            break;
    
        case '60':
            if(time > 60) {                
                objetoLouco.calculatedPrice = price * (time - 60);
                objetoLouco.calculatedPrice = objetoLouco.calculatedPrice + ((objetoLouco.calculatedPrice) * 10)/100;
            }            
            objetoLouco.otherPrice = (price * time)  
            break
        case '120':
            if(time > 120){
                objetoLouco.calculatedPrice = price * (time - 120);
                objetoLouco.calculatedPrice = objetoLouco.calculatedPrice + ((objetoLouco.calculatedPrice) * 10)/100
            }
            objetoLouco.otherPrice = (price * time)  
            break
    }

    return objetoLouco;
}

export {calculatePrice, returnDDDPrice }
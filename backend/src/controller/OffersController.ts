import {Request, Response} from 'express';
import {returnDDDPrice, calculatePrice} from '../utils/priceCalculator';

interface ScheduleItem {
    origin: string;
    destiny: string;
    time: string;
    offers: string;
}

export default class OffersController {
    async index( request: Request, response: Response) {
        const filters = request.query;
        const origin = filters.origin as string
        const destiny = filters.destiny as string
        const time = filters.time as string
        const offers = filters.offers as string        
        const price = returnDDDPrice(origin, destiny);

        return response.json(calculatePrice(price, Number.parseInt(time), offers))
    }

}